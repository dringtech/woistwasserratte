-- Up
CREATE TABLE TrackPoint (
  id INTEGER PRIMARY KEY,
  messengerId TEXT,
  dateTime TEXT,
  latitude FLOAT,
  longitude FLOAT);

-- Down
DROP TABLE TrackPoint;

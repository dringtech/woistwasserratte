#!/usr/bin/env node

const store = require('./lib/store');
const process = require('./lib/process');
const {promisify} = require('util');
const readFile = promisify(require('fs').readFile);

readFile('archive/tracklog-1529558660186.json')
  .then(JSON.parse)
  .then(async (data) => {
    await store.openDb('./tracklog.sqlite');
    store.saveTrack(data);
    return store.readTrack();
  })
  .then(async (data) => {
    await store.closeDb();
    return data;
  })
  .then(process);

#!/usr/bin/env node

const feedReader = require('./lib/feedread');
const process = require('./lib/process');
const store = require('./lib/store');

feedReader(false)
  .then(async (data) => {
    await store.openDb('./tracklog.sqlite');
    store.saveTrack(data);
    return store.readTrack();
  })
  .then(async (data) => {
    await store.closeDb();
    return data;
  })
  .then(process);

const request = require('request-promise-native');
const {promisify} = require('util');
const fs = require('fs');

const writeFile = promisify(fs.writeFile);

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

class Feeder {
  constructor(feedId) {
    this.feedId = feedId;
  }
  url(start=1, limit=50, type='json') {
    return `https://api.findmespot.com/spot-main-web/consumer/rest-api/2.0/public/feed/${this.feedId}/message.${type}?start=${start}&limit=${limit}`
  }
  read(start=1, limit=50) {
    console.log(`Starting ${start} for ${limit}`)
    return request(this.url(start, limit))
      .then((res) => {
        return JSON.parse(res);
      })
      .then(async (res) => {
        if (res.response.hasOwnProperty('errors')) {
          return [];
        } else {
          await sleep(2000);
          return Promise.all([this.read(start+limit,limit), res.response.feedMessageResponse.messages.message])
            .then((messages) => {
              return messages[0].concat(messages[1]);
            });
        }
      });
  }
}

const feeder = new Feeder("0NUfrHlivo522ApxxlYYvqmocLhPgMvdD");

module.exports = (grab=false) => {
  if (!grab) {
    return Promise.resolve([]);
  }
  return feeder.read()
  .then((x) => {
    console.log(`Retrieved ${x.length} records`);
    return x;
  })
  .then((data) => {
    writeFile(`archive/tracklog-${Date.now()}.json`, JSON.stringify(data));
    return data
  })
  .catch(console.error);
}

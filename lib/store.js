const sqlite = require('sqlite');

let trackDb = null;

function openDb(dbName) {
  return sqlite.open(dbName)
    .then((db) => { return db.migrate() })
    .then((db) => { return trackDb = db })
    .catch(console.error);
}

function closeDb() {
  return trackDb.close()
    .catch(console.error);
}

function saveTrack(data) {
  return Promise.all(
    data.map((r) => {
      trackDb.run(
        `INSERT OR IGNORE INTO TrackPoint
          ( id,
            messengerId,
            dateTime,
            latitude,
            longitude
          )
        VALUES
          ( ${r.id},
            "${r.messengerId}",
            "${r.dateTime}",
            ${r.latitude},
            ${r.longitude}
          )`
      )
    })
  )
  .catch(console.error);
}

function readTrack() {
  return trackDb.all(`SELECT * FROM TrackPoint ORDER BY dateTime`);
}

module.exports = {
  openDb: openDb,
  closeDb: closeDb,
  saveTrack: saveTrack,
  readTrack: readTrack,
}

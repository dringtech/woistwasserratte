const {promisify} = require('util');
const writeFile = promisify(require('fs').writeFile);

function getLatLng(trackPoint) {
  // Turn into points. Not sure we can trust altitude?
  return [trackPoint.longitude, trackPoint.latitude];
};

module.exports = (data) => {
  new Promise((resolve, reject) => {
    resolve(data);
  })
    .then((data) => {
      return data.sort((a,b) => { return Date.parse(a.dateTime) - Date.parse(b.dateTime); })
    })
    .then((data) => {
      const dayTracks = data.reduce((dayData, x) => {
        const date = x.dateTime.split('T')[0];
        if (! dayData.hasOwnProperty(date)) dayData[date] = {
          coords: [],
          startTime: Date.parse(x.dateTime),
          endTime: Date.parse(x.dateTime),
        };
        dayData[date].coords.push(getLatLng(x));
        dayData[date].startTime = Math.min(Date.parse(x.dateTime), dayData[date].startTime);
        dayData[date].endTime = Math.max(Date.parse(x.dateTime), dayData[date].startTime);
        return dayData;
      }, {});
      return Object.getOwnPropertyNames(dayTracks).map((day) => {
        return {
          date: day,
          startTime: new Date(dayTracks[day].startTime).toTimeString(),
          endTime: new Date(dayTracks[day].endTime).toTimeString(),
          track: dayTracks[day].coords,
        }
      });
    })
    .then((data) => {
      const geoJson = {
        "type": "FeatureCollection",
        "features": data.map((x, idx) => {
          return {
            "type": "FeatureCollection",
            "features": [
              {
                "type": "Feature",
                "geometry": { "type": "LineString", "coordinates": x.track },
                "properties": { "date": x.date, "colour": idx % 2 }
              },
              {
                "type": "Feature",
                "geometry": { "type": "Point", "coordinates": x.track[0] },
                "properties": { "date": x.date, "type": "start", "time": x.startTime }
              },
              {
                "type": "Feature",
                "geometry": { "type": "Point", "coordinates": x.track[x.track.length-1] },
                "properties": { "date": x.date, "type": "end", "time": x.endTime }
              }
            ]
            }
        })
      }
      return geoJson;
    })
    .then((geoJson) => {
      return writeFile('emma.js', `const emma=${JSON.stringify(geoJson)};`);
    })
    .catch((error) => {
      console.error(error);
    })
}
